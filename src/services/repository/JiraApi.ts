import axios from "axios";


const baseDomain = "baseDomainGoesHere";
const baseURL = `${baseDomain}`;

export default axios.create({
    baseURL
});