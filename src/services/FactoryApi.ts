//Our APi factory

import JiraApi from "./JiraTicketsRepository";


const repositories: any = {
    jiraApi: JiraApi
};
export default {
    get: (name: string | number) => {
        return repositories[name];
    }
};