/// Example set up for getting Jira data

import JiraApi from "./repository/JiraApi";

const resource = "api/endpoint";
export default {
    getAll() {
        return JiraApi.get(`${resource}`);
    },
    getOne(id: any) {
        return JiraApi.get(`${resource}/${id}`);
    },

    searchAll(query: any) {
        return JiraApi.get(`${resource}?s=${query}`);
    }
};