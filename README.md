# jira-desktop-tool

A single page Vue application that interacts with Jira

Uses:

~~Inversify Props: Dependancy Injection
~~This package is a wrapper of Inversify to simplify how inject your dependencies with property decorators in the components, made with TypeScript and compatible with Vue, React and other component libraries.

Boostrap: for my sins

Axios: works better with the factory pattern

Run Jest Tests with npm test -- --watch .... to make life easy

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Run your unit tests

```
npm run test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# Jira Desktop Tool
